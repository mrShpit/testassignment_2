﻿Shader "Unlit/OutlineShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color("Color", Color) = (0,0,0,1)
		_OutlineColor("Outline color", Color) = (0,0,0,1)
		_OutlineWidth ("Width", Range(1.0, 1.5)) = 1.0
		
	}

	CGINCLUDE
	#include "UnityCG.cginc"

	struct screendata
	{
		float4 vertex : POSITION;
		float3 normal : NORMAL;
	};

	struct v2f
	{
		float4 pos : POSITION;
		float3 normal : NORMAL;
	};

	float _OutlineWidth;
	float4 _OutlineColor;

	v2f vert(screendata sd)
	{
		sd.vertex.xyz *= _OutlineWidth;
		v2f p;
		p.pos = UnityObjectToClipPos(sd.vertex);
		return p;
	};

	ENDCG

	SubShader
	{
		Pass
		{
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			half4 frag(v2f p) : COLOR
			{
				return _OutlineColor;
			}
			ENDCG

		}
		Pass
		{
			ZWrite On
			Lighting On

			Material
			{
				Diffuse[_Color]
				Ambient[_Color]
			}

			SetTexture[_MainTex]
			{
				ConstantColor[_Color]
			}

			SetTexture[_MainTex]
			{
				Combine previous * primary DOUBLE
			}
		}
	}
}
