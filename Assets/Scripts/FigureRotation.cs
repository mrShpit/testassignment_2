﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureRotation : MonoBehaviour
{
    private Transform myTransform;

	void Start ()
    {
        myTransform = this.transform;
	}
	
	void Update ()
    {
        myTransform.Rotate(1, 1, 0);
	}
}
